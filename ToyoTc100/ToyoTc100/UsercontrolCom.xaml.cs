﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Ports;
using Microsoft.Win32;

namespace ToyoTc100
{
    /// <summary>
    /// UsercontrolCom.xaml 的互動邏輯
    /// </summary>
    public partial class UsercontrolCom : UserControl
    {

        public UsercontrolCom()
        {

            InitializeComponent();
            Searchcomport();
            //MainWindow mainform = new MainWindow();
            //mainform.Transfcominfo += frm_Transfcom;
        }
        public delegate void TransfcomDelegate(string _com, string _baudrate);
        public TransfcomDelegate Transfcominfo;
        private void Searchcomport()
        {

            cb_tc100com.Items.Clear();
            cb_arduinocom.Items.Clear();
            RegistryKey keyCom = Registry.LocalMachine.OpenSubKey("Hardware\\DeviceMap\\SerialComm");
            string[] sSubKeys = keyCom.GetValueNames();
            Array.Sort(sSubKeys);
            int _com_num = sSubKeys.Count();
            foreach (string sName in sSubKeys)
            {
                string sValue = (string)keyCom.GetValue(sName);
                cb_tc100com.Items.Add(sValue);
                cb_arduinocom.Items.Add(sValue);

            }
            cb_tc100com.SelectedIndex = 0;
            cb_arduinocom.SelectedIndex = 0;

        }


        private void Bt_tc100com_Click(object sender, RoutedEventArgs e)
        {
           
            //MessageBox.Show("!1");
        }
        private void Bt_arduinocom_Click(object sender, RoutedEventArgs e)
        {
            var main = App.Current.MainWindow as MainWindow;
        }


    }

}
