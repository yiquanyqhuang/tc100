﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Input;
//these two namespace are required 
//using Microsoft.Office.Interop.Excel;
using System.Collections.ObjectModel;
using System.Xml.Serialization;

namespace ToyoTc100
{
    /// <summary>
    /// Interação lógica para UserControlInicio.xam
    /// </summary>
    public partial class UserControlInicio : UserControl
    {
        string bootpath, configpath, PathScript_pre, PathScript_run, todatafile = null;
        List<ToyoFuncChara> DataGriditems;
        int MaxStride = 0;
        Usercontroltable SetForm;

        public UserControlInicio()
        {
            InitializeComponent();
            List<resultdataformat> items = new List<resultdataformat>();
             SetForm = new Usercontroltable();
            bootpath = System.IO.Directory.GetCurrentDirectory();

            PathScript_pre = bootpath + @"\script\_pra\";
            PathScript_run = bootpath + @"\script\_run\";
            todatafile = bootpath + @"\data";

            if (Directory.Exists(bootpath + @"\param\_run") == true)
            {
                configpath = string.Concat(bootpath, @"\param\_run\config.ini");
                loadscript(configpath);
            }

            //loading script
            LoadingScriptlist();
            SetScriptGrid.Visibility = Visibility.Hidden;
        }

        private void LoadingScriptlist()
        {

            DirectoryInfo di = new DirectoryInfo(PathScript_run);
            Listscripts.Items.Clear();
            foreach (var fi in di.GetFiles("*.ini"))
            {
                Listscripts.Items.Add(fi.Name.Replace(".ini", ""));
            }


        }



        private void bt_exportscript_Click(object sender, RoutedEventArgs e)
        {
            LoadingScriptlist();
            if (!Listscripts.Items.Contains(tb_scriptname.Text))
                Savescriptfile();
            else
            {
                MessageBoxResult result = MessageBox.Show("Do you want to override this file?2", "Confirmation ~~", MessageBoxButton.YesNo);
                if (result == MessageBoxResult.Yes)Savescriptfile();

            }
            System.Diagnostics.Process prc = new System.Diagnostics.Process();
            prc.StartInfo.FileName = PathScript_run;
            prc.Start();

        }

        void Savescriptfile()
        {

            string NewIni_name = string.Concat(tb_scriptname.Text, ".ini");
            SaveDataToIni(dataGrid1, PathScript_run + NewIni_name);

            string New_nameXml = string.Concat(tb_scriptname.Text, ".xml");
            SaveDataToXml(dataGrid1, PathScript_run + New_nameXml);

            string New_nameTxt = string.Concat(tb_scriptname.Text, ".txt");
            SaveDataToTxt(dataGrid1, PathScript_run + New_nameTxt);

        }


        //save DataGrid data to ini
        private void SaveDataToIni(DataGrid _obj, string _path)
        {
            // get var for items
            var meals = _obj.Items;
            //convert to list
            List<ToyoFuncChara> NewMeals = meals.OfType<ToyoFuncChara>().ToList();
            //convert list to ObservableCollection
            var oc = new ObservableCollection<ToyoFuncChara>(NewMeals);

            IniFile iniManager = new IniFile(_path);

            for (int i = 0; i < oc.Count; i++)
            {
                //operation_mode
                //moving_coordinate
                //moving_speed
                //wait_time
                //force
                //beused
                string idstr = (i + 1).ToString();
                string operation_mode = oc[i].operation_mode.ToString();


                iniManager.WriteIniFile(idstr, "operation_mode", operation_mode);
                iniManager.WriteIniFile(idstr, "moving_coordinate", oc[i].moving_coordinate.ToString());
                iniManager.WriteIniFile(idstr, "moving_speed", oc[i].moving_speed.ToString());
                iniManager.WriteIniFile(idstr, "wait_time", oc[i].wait_time.ToString());
                iniManager.WriteIniFile(idstr, "force", oc[i].force.ToString());
                iniManager.WriteIniFile(idstr, "beused", oc[i].beused.ToString());
                iniManager.WriteIniFile(idstr, "Record", oc[i].Record.ToString());

            }
        }

        /// <summary>
        /// save DataGrid data to xml
        /// <para>
        ///the code reference  <see href="https://github.com/Wolfleader101/DinnerPlanner/blob/b55aeac590a5cb422714ed3a11b51055009b0743/MainWindow.xaml.cs"></see>
        /// </para>
        /// </summary>
        /// <param name="_obj"></param>
        /// <param name="path"></param>
        /// 
        private void SaveDataToXml(DataGrid _obj, string path)
        {
            // get var for items
            var meals = _obj.Items;
            //convert to list
            List<ToyoFuncChara> NewMeals = meals.OfType<ToyoFuncChara>().ToList();
            //convert list to ObservableCollection
            var oc = new ObservableCollection<ToyoFuncChara>(NewMeals);
            //serialize to XML
            XmlSerializer xs = new XmlSerializer(typeof(ObservableCollection<ToyoFuncChara>));
            using (StreamWriter wr = new StreamWriter(path))
            {
                //write to file
                xs.Serialize(wr, oc);
            }
        }

        //save DataGrid data to txt
        private void SaveDataToTxt(DataGrid _obj, string _path)
        {
            _obj.SelectAllCells();
            _obj.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
            ApplicationCommands.Copy.Execute(null, _obj);
            _obj.UnselectAllCells();
            string result1 = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
            Clipboard.Clear();
            System.IO.StreamWriter file1 = new System.IO.StreamWriter(_path);
            file1.WriteLine(result1);
            file1.Close();
            //MessageBox.Show("OK");
        }

        private void bt_savedatatoCSV_Click(object sender, RoutedEventArgs e)
        {
            SaveDataToCSV(lv_resultdata, true);

            //gv_resultdata
        }




        private void bt_importscript_Click(object sender, RoutedEventArgs e)
        {
            LoadingScriptlist();
            SetScriptGrid.Visibility = Visibility.Visible;
        }


        //public class resultdataformat
        //{
        //    public string time { get; set; }
        //    public int loop { get; set; }
        //    public int stride { get; set; }
        //    public string parm1 { get; set; }
        //    public string note { get; set; }
        //}
        private void bt_deletedata_Click(object sender, RoutedEventArgs e)
        {
            lv_resultdata.ItemsSource = null;

            for (int i = 0; i < 1000000; i++)
            {
                List<resultdataformat> ListViewData = new List<resultdataformat>();
                string daytime = System.Text.RegularExpressions.Regex.Replace(DateTime.Now.ToShortDateString().ToString(), "[^0-9]", "");
                string time = DateTime.Now.TimeOfDay.ToString();
                time = time.Substring(0, 12).Replace(":", "");
                ListViewData.Add(new resultdataformat { time = daytime+ time, loop = i, stride=i, parm1 = "FE 05 39 00 DE FE 05 39 00", note = "FE 05 39 00 DE FE 05 39 00" });
                lv_resultdata.Items.Add(ListViewData);
            }

        }

        private void bt_Runschadule_Click(object sender, RoutedEventArgs e)
        {
            string _selectcom = serialparam.toyo_name;
            int _delay = 0;
            string _return = null;
        
            //Send_data(scriptCMD.SlowDown, _selectcom, _delay, _return, false);   
            //Send_data(scriptCMD.ORG, _selectcom, _delay, _return, false);
            RunProgram();


        }

        private void bt_Stopschadule_Click(object sender, RoutedEventArgs e)
        {
            string _selectcom = serialparam.toyo_name;
            int _delay = 0;
            string _return = null;
            //MessageBox.Show(scriptCMD.SlowDown);
            Send_data(scriptCMD.SlowDown, _selectcom, _delay, _return,true);
        }

        private void bt_check_Click(object sender, RoutedEventArgs e)
        {
            string selectfile = Listscripts.SelectedValue.ToString();
            selectfile = PathScript_run + selectfile + ".ini";
            loadscript(selectfile);
            SetScriptGrid.Visibility = Visibility.Hidden;
        }

        private void bt_cancel_Click(object sender, RoutedEventArgs e)
        {
            SetScriptGrid.Visibility = Visibility.Hidden;
        }

        private void bt_savefiletocsv_Click(object sender, RoutedEventArgs e)
        {
            SaveDataGridDatatoCSV(dataGrid1, todatafile);
            MessageBox.Show("Changes have been successfully saved to an XML file.", "Info");
        }

        private void SaveDataToCSV(ListView _lvUsers, bool titleon = true, bool fileopen = false)
        {

            StringBuilder sb = new StringBuilder();
            GridView gv = _lvUsers.View as GridView;

            if (titleon)
            {
                sb.Append("Time,Loop,Stride,Data,Note");
                sb.Append(Environment.NewLine);
            }

            for (int i = 0; i < _lvUsers.Items.Count; ++i)
            {
                Type t = _lvUsers.Items[i].GetType();

                foreach (GridViewColumn gc in gv.Columns)
                {

                    string bindingPath = (gc.DisplayMemberBinding as Binding).Path.Path;
                    PropertyInfo pi = _lvUsers.Items[i].GetType().GetProperty(bindingPath);
                    string value = pi.GetValue(_lvUsers.Items[i]).ToString();
                    sb.Append(value);
                    sb.Append(",");
                }
                sb.Append(Environment.NewLine);
            }
            System.IO.File.WriteAllText(todatafile + @"\data1.csv", sb.ToString());


            if (fileopen)
            {
                System.Diagnostics.Process prc = new System.Diagnostics.Process();
                prc.StartInfo.FileName = todatafile;
                prc.Start();
            }

        }

        /// <summary>
        /// loads the ini datat to import datagrid
        /// <para> ID <remarks>string</remarks> </para>
        /// <para> operation_mode <remarks>int</remarks> </para>
        /// <para> moving_coordinate <remarks>string</remarks> </para>
        /// <para> moving_speed <remarks>string</remarks> </para>
        /// <para> wait_time <remarks>string</remarks> </para>
        /// <para> force <remarks>string</remarks> </para>
        /// <para> beused <remarks>bool</remarks> </para>
        /// <para> Record <remarks>bool</remarks> </para>
        /// </summary>
        private void loadscript(string _path)
        {
            IniFile iniManager = new IniFile(_path);
            DataGriditems = new List<ToyoFuncChara>();
            MaxStride = 0;
            for (int i = 0; i < 60; i++)
            {
                string idstr = (i + 1).ToString();
                string operation_mode = iniManager.ReadIniFile(idstr, "operation_mode", "default");
                string WRITEbeused = iniManager.ReadIniFile(idstr, "beused", "default").ToLower();
                string WRITERecord = iniManager.ReadIniFile(idstr, "Record", "default").ToLower();

                if (string.IsNullOrEmpty(WRITEbeused))
                    WRITEbeused = "0";

                if (MaxStride < Convert.ToInt32(WRITEbeused))
                    MaxStride = Convert.ToInt32(WRITEbeused);

                DataGriditems.Add(new ToyoFuncChara
                {
                    ID = (i + 1),
                    operation_mode = Convert.ToInt32(operation_mode),
                    moving_coordinate = iniManager.ReadIniFile(idstr, "moving_coordinate", "default"),
                    moving_speed = iniManager.ReadIniFile(idstr, "moving_speed", "default"),
                    wait_time = iniManager.ReadIniFile(idstr, "wait_time", "default"),
                    force = iniManager.ReadIniFile(idstr, "force", "default"),
                    beused = Convert.ToInt32(WRITEbeused),
                    Record = WRITERecord == "true" ? true : false


                });

            }

            dataGrid1.ItemsSource = DataGriditems;

        }

        /// <summary>
        /// this https://github.com/ins1gn1a/Pwdlyser/blob/b84c635b4b319f9963ba38ef94b51a405f990929/Pwdlyser/MainWindow.xaml.cs
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void bt_savescript_Click(object sender, RoutedEventArgs e)
        {
            TransfCommandTC transf = new TransfCommandTC();
            string _selectcom = serialparam.toyo_name;

            // get var for items
            var meals = dataGrid1.Items;
            //convert to list
            List<ToyoFuncChara> NewMeals = meals.OfType<ToyoFuncChara>().ToList();
            //convert list to ObservableCollection
            var oc = new ObservableCollection<ToyoFuncChara>(NewMeals);


            scriptCMD.Dic_GetModemsg = new Dictionary<int, byte[]>();
            scriptCMD.Dic_GetDistancemsg = new Dictionary<int, byte[]>();
            scriptCMD.Dic_GetSpeed = new Dictionary<int, byte[]>();
            scriptCMD.Dic_GetDelaytime = new Dictionary<int, byte[]>();
            scriptCMD.Dic_GetNextStride = new Dictionary<int, byte[]>();


            for (int i = 0; i < oc.Count; i++)
            {

                if (oc[i].beused != 0)
                {
                    transf.stride_index = i + 1;
                    int _ide = i + 1;
                    //operation_mode    moving_coordinate   moving_speed    wait_time   force   beused


                    string operation_mode = oc[i].operation_mode.ToString();
                    transf.stride_Mode = operation_mode;

                    transf.stride_distance = oc[i].moving_coordinate.ToString();
                    transf.stride_speed = oc[i].moving_speed.ToString();
                    transf.stride_delaytime = oc[i].wait_time.ToString();
                    transf.stride_force = oc[i].force.ToString();
                    transf.stride_nextstride = oc[i].beused.ToString();



                    //save the param to the main form
                    string str_GetModemsg = System.Text.Encoding.UTF8.GetString(transf.GetModemsg());
                    string str_GetDistancemsg = System.Text.Encoding.UTF8.GetString(transf.GetDistancemsg());
                    string str_GetSpeed = System.Text.Encoding.UTF8.GetString(transf.GetSpeed());
                    string str_GetDelaytime = System.Text.Encoding.UTF8.GetString(transf.GetDelaytime());
                    string str_GetNextStride = System.Text.Encoding.UTF8.GetString(transf.GetNextStride());

                    MessageBox.Show(str_GetModemsg);
                    MessageBox.Show(str_GetDistancemsg);
                    MessageBox.Show(str_GetSpeed);
                    MessageBox.Show(str_GetDelaytime);
                    MessageBox.Show(str_GetNextStride);

                    scriptCMD.Dic_GetModemsg.Add(_ide, transf.GetModemsg());
                    scriptCMD.Dic_GetDistancemsg.Add(_ide, transf.GetDistancemsg());
                    scriptCMD.Dic_GetSpeed.Add(_ide, transf.GetSpeed());
                    scriptCMD.Dic_GetDelaytime.Add(_ide, transf.GetDelaytime());
                    scriptCMD.Dic_GetNextStride.Add(_ide, transf.GetNextStride());


                }


            }

        }


        public static byte[] HexStringToBytes(string hs)
        {
            byte[] a = { 0x00 };            //非靜態變量處理
            try
            {
                //byte[] mid = Encoding.Default.GetBytes(hs);
                string[] strArr = hs.Trim().Split(' ');
                byte[] b = new byte[strArr.Length];
                //逐字變為16進制

                foreach (var word in strArr.Select((value, index) => new { value, index }))
                {
                    b[word.index] = Convert.ToByte(strArr[word.index].PadLeft(2, '0'), 16);
                }
                return b;
            }
            catch (Exception)
            {
                MessageBox.Show("重新確認輸入的16進制(please check format that the command input)");
                return a;
            }
        }

        /// <summary>
        /// let msg into the method then procesing msg, and sending the msg to Toyo or other serial
        /// <see href= "https://igouist.github.io/post/2020/07/code-style/">良好寫手備忘錄</see>
        /// <para><typeparamref name="string"/> , <paramref name="data"/>  msge</para>
        /// <para><typeparamref name="string"/> , <paramref name="_com"/>  msge</para>
        /// <para><typeparamref name="int"/> , <paramref name="_delay"/>  msge</para>
        /// <para><typeparamref name="string"/> , <paramref name="_return"/>  msge</para>
        /// </summary>
        private void SaveDataGridDatatoCSV(DataGrid DataGridUserProperties, string _path)
        {

            try
            {

                SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
                dlg.InitialDirectory = _path;
                dlg.Title = "Save the data to ...";
                dlg.DefaultExt = ".csv";
                dlg.Filter = "Comma-Separated File (CSV)|*.csv";

                if (dlg.ShowDialog() == true)
                {
                    string filename = dlg.FileName;
                    DataGridUserProperties.SelectAllCells();
                    DataGridUserProperties.ClipboardCopyMode = DataGridClipboardCopyMode.IncludeHeader;
                    ApplicationCommands.Copy.Execute(null, DataGridUserProperties);
                    DataGridUserProperties.UnselectAllCells();

                    string result = (string)Clipboard.GetData(DataFormats.CommaSeparatedValue);
                    File.AppendAllText(filename, result, UnicodeEncoding.UTF8);
                }

            }
            catch
            {
                MessageBox.Show("Please retrieve user details from Active Directory before saving to CSV.");
            }

        }
        public void Send_data(string data, string _com, int _delay = 0, string _return = null, bool debug = false)
        {

            string msg = "";
            try
            {
                msg = data;
                msg = string.Concat(msg, "0D 0A");

                if (debug)
                    MessageBox.Show(msg);

                else
                {
                    if (serialparam.Coms[_com].IsOpen)
                    {
                        //半段是否是hex
                        if (true)
                        {
                            byte[] hex = HexStringToBytes(msg);
                            serialparam.Coms[_com].Write(hex, 0, hex.Length);
                        }
                        else
                        {
                            byte[] UTF8bytes = Encoding.UTF8.GetBytes(msg);
                            serialparam.Coms[_com].Write(UTF8bytes, 0, UTF8bytes.Length);

                        }

                        if (_return != null)
                            System.Threading.Thread.Sleep(_delay);

                    }

                }

            }
            catch (Exception ex)
            {
                MessageBox.Show("send_data error! ", "錯誤提醒");
            }


        }
        public void RunProgram()
        {



        }

    }

    public class ToyoFuncChara
    {
        public int ID { get; set; }
        private static Dictionary<int, Operation_mode> getAll;
        static ToyoFuncChara()
        {
            getAll = new Dictionary<int, Operation_mode>()
            {
                { 0, new Operation_mode {  Name  = "INC" } },
                { 1, new Operation_mode {  Name  = "ABS" } },
                { 2, new Operation_mode {  Name  = "ORG" } },
                { 3, new Operation_mode {  Name  = "TLS+" } },
                { 4, new Operation_mode {  Name  = "TLS-" } },
                { 5, new Operation_mode {  Name  = "INC-R" } },
                { 6, new Operation_mode {  Name  = "ABS-R" } },
                { 7, new Operation_mode {  Name  = "INC-T" } },
                { 8, new Operation_mode {  Name  = "ABS-T" } }

            };
        }
        public static Dictionary<int, Operation_mode> GetAll
        {
            get { return getAll; }
        }
        public int operation_mode { get; set; }
        public string Name { get; set; }
        public string moving_coordinate { get; set; }
        public string moving_speed { get; set; }
        public string wait_time { get; set; }
        public string force { get; set; }
        public int beused { get; set; }
        public bool Record { get; set; }


    }
    public class resultdataformat
    {
        public string time { get; set; }
        public int loop { get; set; }
        public int stride { get; set; }
        public string parm1 { get; set; }
        public string note { get; set; }
    }

    public class Operation_mode : List<string>
    {
        public string Name { get; set; }
    }

    public class IniFile
    {
        private string filePath;
        private StringBuilder lpReturnedString;
        private int bufferSize;

        [DllImport("kernel32")]
        private static extern long WritePrivateProfileString(string section, string key, string lpString, string lpFileName);

        [DllImport("kernel32")]
        private static extern int GetPrivateProfileString(string section, string key, string lpDefault, StringBuilder lpReturnedString, int nSize, string lpFileName);

        public IniFile(string iniPath)
        {
            filePath = iniPath;
            bufferSize = 512;
            lpReturnedString = new StringBuilder(bufferSize);
        }

        // read ini date depend on section and key
        public string ReadIniFile(string section, string key, string defaultValue)
        {
            lpReturnedString.Clear();
            GetPrivateProfileString(section, key, defaultValue, lpReturnedString, bufferSize, filePath);
            return lpReturnedString.ToString();
        }

        // write ini data depend on section and key
        public void WriteIniFile(string section, string key, Object value)
        {
            WritePrivateProfileString(section, key, value.ToString(), filePath);
        }

    }


}
