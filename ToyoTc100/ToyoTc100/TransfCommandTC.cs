﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyoTc100
{
    public class TransfCommandTC
    {
        //operation_mode
        //moving_coordinate
        //moving_speed
        //wait_time
        //force
        //beused
       
        FuncPackage Package = new FuncPackage();
        public string stride_Mode, stride_distance, stride_delaytime, stride_speed, stride_force, stride_beused, stride_nextstride, loopindex=null;
        public int stride_endindex, stride_index ;
        public string ASCIIToHex(string ascii)
        {
            StringBuilder sb = new StringBuilder();
            byte[] inputBytes = Encoding.UTF8.GetBytes(ascii);
            foreach (byte b in inputBytes)
            {
                sb.Append(string.Format("{0:X2} ", b));
            }
            return sb.ToString();
        }
        public byte[] GetModemsg()
        {
            //
            string MODEindex = indexconverthex(stride_index);
            //30 36 39 30 31 30 30 30 30 3x;
            byte[] bytesmsg = System.Text.Encoding.Default.GetBytes(stride_Mode.ToString());
            string result = System.Text.Encoding.UTF8.GetString(bytesmsg).PadLeft(2, '3');//31
            string modeStr_sub1 = string.Concat("30 36 39 ", MODEindex, "30 30 30 30 ");
            string modeStr = string.Concat(modeStr_sub1, result);              
            //modeStr = frm.ToyoASCII(modeStr);
            modeStr = Package.CheckSum82sComple(modeStr ) + "0D 0A";
            byte[] bytemsg = modeStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;
        }
        public byte[] GetDistancemsg()
        {
            //
            string Disindex = indexconverthex(stride_index);
            //
            double distancedouble = double.Parse(stride_distance) * 100;
            string distance = distancedouble.ToString();
            distance = Convert.ToString(Int32.Parse(distance), 16).ToUpper();
            distance = ASCIIToHex(distance).Trim();
            string distancecommand;
            int distanceInt = (int)distancedouble;
            if (distanceInt >= 4096) distancecommand = string.Concat("31 30 39 ", Disindex, "31 30 30 30 32 30 34 30 30 30 30 ", distance);
            else if (distanceInt < 4096 && distanceInt >= 256)
                distancecommand = string.Concat("31 30 39 ", Disindex, "31 30 30 30 32 30 34 30 30 30 30 30 " + distance);
            else if (distanceInt < 256 && distanceInt >= 16)
                distancecommand = string.Concat("31 30 39 ", Disindex, "31 30 30 30 32 30 34 30 30 30 30 30 30 " + distance);
            else
                distancecommand = string.Concat("31 30 39 ", Disindex, "31 30 30 30 32 30 34 30 30 30 30 30 30 30 " + distance);
            string disStr = Package.CheckSum82sComple(distancecommand ) + "0D 0A";
            byte[] bytemsg = disStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;
        }
        public byte[] GetDelaytime()
        {
            //
            string Delaytimeindex = indexconverthex(stride_index);
            //
            string delaytime = Convert.ToString(Int32.Parse(stride_delaytime), 16).ToUpper();
            delaytime = ASCIIToHex(delaytime).Trim();
            string delaytimecommand;
            int delayInt = Int32.Parse(stride_delaytime);
            if (delayInt >= 4096)
                delaytimecommand = string.Concat("30 36 39 ", Delaytimeindex, "43 " + delaytime);
            else if (delayInt < 4096 && delayInt >= 256)
                delaytimecommand = string.Concat("30 36 39 ", Delaytimeindex, "43 30 " + delaytime);
            else if (delayInt < 256 && delayInt >= 16)
                delaytimecommand = string.Concat("30 36 39 ", Delaytimeindex, "43 30 30 " + delaytime);
            else
                delaytimecommand = string.Concat("30 36 39 ", Delaytimeindex, "43 30 30 30 " + delaytime);
            //      string delaytimeStr = frm.ToyoASCII(delaytimecommand.Trim());
            string delaytimeStr = Package.CheckSum82sComple(delaytimecommand ) + "0D 0A";
            byte[] bytemsg = delaytimeStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;

        }
        public byte[] GetSpeed()
        {
            //30 36 39 30 31 33 30 30 36 34
            string Speedindex = indexconverthex(stride_index);
            //
            string speedtime = Convert.ToString(Int32.Parse(stride_speed), 16).ToUpper();
            speedtime = ASCIIToHex(speedtime).Trim();
            string speedtimecommand;
            int speedtimeInt = speedtime.Length;
            if (speedtimeInt <= 2)
                speedtimecommand = string.Concat("30 36 39 ", Speedindex, "33 30 30 30 " + speedtime);
            else
                speedtimecommand = string.Concat("30 36 39 ", Speedindex, "33 30 30 " + speedtime);

            string speedtimeStr = Package.CheckSum82sComple(speedtimecommand ) + "0D 0A";
            byte[] bytemsg = speedtimeStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;

        }
        public byte[] GetNextStride()
        {
            //
            string NextStrideindex = indexconverthex(stride_index);
            //
            //30 36 39 30 31 30 30 30 30 3x;
            int IntStride_nextstride = int.Parse(stride_nextstride);
            string result = indexconverthex(IntStride_nextstride);
            //31 30
            string modeStr_sub1 = string.Concat("30 36 39 ", NextStrideindex, "44 30 30 ");

            string modeStr = string.Concat(modeStr_sub1, result.Trim());
        
            modeStr = Package.CheckSum82sComple(modeStr ) + "0D 0A";
            byte[] bytemsg = modeStr.Split().Select(s => Convert.ToByte(s, 16)).ToArray();
            return bytemsg;

        }
        private string indexconverthex(int stride_index)
        {

            string strnum = stride_index.ToString();
            //
            //  int 1 = 30 31
            //  int 11 = 31 31
            //  int 127 = 3c 37
            //

            if (strnum.Length == 1)
            {
                string str = Convert.ToString(stride_index, 16).ToUpper().PadLeft(2, '3');
                str = string.Concat("30 ", str, " ");        //30 3x
                return str;
            }
            else if (strnum.Length == 2)
            {
                string str = ASCIIToHex(stride_index.ToString());
                str = string.Concat(str.Trim(), " ");        //30 3x
                return str;
            }
            else
            {
                int prefix = int.Parse(strnum.Substring(0, 2));
                string str0 = Convert.ToString(prefix, 16).ToUpper().PadLeft(2, '3');
                string str1 = Convert.ToString(int.Parse(strnum.Substring(2, 1)), 16).ToUpper().PadLeft(2, '3');
                string str = string.Concat(str0, " ", str1, " ");        //30 3x
                return str;
                //MessageBox.Show("~" + str2 + "~");
            }

        }


    }
}
