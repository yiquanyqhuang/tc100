﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Ports;
using Microsoft.Win32;
using System.Windows.Threading;
using System.Threading;

namespace ToyoTc100
{
    /// <summary>
    /// MainWindow.xaml 的互動邏輯
    /// </summary>
    ///
    public class serialparam
    {
        public static string arduino_name = null;
        public static string toyo_name = null;
        public static int arduino_baudrate = 0;
        public static int toyo_baudrate = 0;
        public static bool use_arduino = false;
        public static bool use_toyo = false;
        public static bool use_com = false;
        public static string[] sSubKeys ;

        public static Dictionary<string, SerialPort> Coms;
        public static Dictionary<string, string[]> Cominfo;
    }

    public class scriptCMD
    {

        public static string FitPos, ORG, SlowDown, BrakedHard, ReSetAlarm, CurrentPos, SimulateSet0, SimulateSet18 = null;
        public static Dictionary<int, string> schedule = new Dictionary<int, string>();

        public static Dictionary<int, byte[]> Dic_GetModemsg = new Dictionary<int, byte[]>();
        public static Dictionary<int, byte[]> Dic_GetDistancemsg = new Dictionary<int, byte[]>();
        public static Dictionary<int, byte[]> Dic_GetSpeed = new Dictionary<int, byte[]>();
        public static Dictionary<int, byte[]> Dic_GetDelaytime = new Dictionary<int, byte[]>();
        public static Dictionary<int, byte[]> Dic_GetNextStride = new Dictionary<int, byte[]>();

        public static Dictionary<string, string> CommonCMD = new Dictionary<string, string>();
        public static List< string> datares ;

    }

    public class WindowForms
    {
        public static UserControlInicio formschadule;

    }

    public partial class MainWindow : Window
    {
        //public SerialPort[] SerialPorts;         //public Dictionary<string, SerialPort> Coms;              //public Dictionary<string, string[]> Cominfo;
        public string indebugpath = System.Environment.CurrentDirectory;
        public int setcompagetoelse = 0;
        public bool Simu_debug  = true;
        public static bool ishex = true;
        ToyohexCMD hCMD = new ToyohexCMD();
        FuncPackage Package = new FuncPackage();

        string time, SendRecMsgstring = null;



        UserControlInicio formschadule = new UserControlInicio();
        Usercontroltable formsetparm = new Usercontroltable();
        DispatcherTimer _timer = new DispatcherTimer();

        public MainWindow()
        {
            InitializeComponent();

            serialparam.Coms = new Dictionary<string, SerialPort>();
            serialparam.Cominfo = new Dictionary<string, string[]>();
            Searchcomport();
            SetParm();

            //宣告Timer
            //設定呼叫間隔時間為30ms
            _timer.Interval = TimeSpan.FromMilliseconds(10);
            //加入callback function
            _timer.Tick += _timer_Tick;
            //開始

        }
        void SetParm()
        {

            scriptCMD.ORG = Package.CheckSum82sComple(string.Concat(ToyohexCMD.STATION, ToyohexCMD.WRITE, ToyohexCMD.NumToHexWord("201E"), ToyohexCMD.NumToHexWord("0003")));
            scriptCMD.ReSetAlarm = Package.CheckSum82sComple(string.Concat(ToyohexCMD.STATION, ToyohexCMD.WRITE, ToyohexCMD.NumToHexWord("201E"), ToyohexCMD.NumToHexWord("0006")));
            scriptCMD.FitPos = Package.CheckSum82sComple(string.Concat(ToyohexCMD.STATION, ToyohexCMD.WRITE, ToyohexCMD.NumToHexWord("201E"), ToyohexCMD.NumToHexWord("0007")));
            scriptCMD.SlowDown = Package.CheckSum82sComple(string.Concat(ToyohexCMD.STATION, ToyohexCMD.WRITE, ToyohexCMD.NumToHexWord("201E"), ToyohexCMD.NumToHexWord("0008")));
            scriptCMD.BrakedHard = Package.CheckSum82sComple(string.Concat(ToyohexCMD.STATION, ToyohexCMD.WRITE, ToyohexCMD.NumToHexWord("201E"), ToyohexCMD.NumToHexWord("0009")));
            //
            scriptCMD.CurrentPos = Package.CheckSum82sComple(string.Concat(ToyohexCMD.STATION, ToyohexCMD.WRITE, ToyohexCMD.NumToHexWord("1008"), ToyohexCMD.NumToHexWord("0002")));
            scriptCMD.SimulateSet0 = Package.CheckSum82sComple(string.Concat(ToyohexCMD.STATION, ToyohexCMD.WRITE, ToyohexCMD.NumToHexWord("2040"), ToyohexCMD.NumToHexWord("0000")));
            scriptCMD.SimulateSet18 = Package.CheckSum82sComple(string.Concat(ToyohexCMD.STATION, ToyohexCMD.WRITE, ToyohexCMD.NumToHexWord("2040"), ToyohexCMD.NumToHexWord("0000")));

            scriptCMD.CommonCMD.Add("ORG", scriptCMD.ORG);
            scriptCMD.CommonCMD.Add("resetalarm", scriptCMD.ReSetAlarm);
            scriptCMD.CommonCMD.Add("fitpos", scriptCMD.FitPos);
            scriptCMD.CommonCMD.Add("slowdown", scriptCMD.SlowDown);
            scriptCMD.CommonCMD.Add("brakedhard", scriptCMD.BrakedHard);
            scriptCMD.CommonCMD.Add("currentpos", scriptCMD.CurrentPos);
        }
        private void Searchcomport()
        {

            //
            RegistryKey keyCom = Registry.LocalMachine.OpenSubKey("Hardware\\DeviceMap\\SerialComm");
            serialparam.sSubKeys = keyCom.GetValueNames();

            serialparam.Coms = new Dictionary<string, SerialPort>();
            Array.Sort(serialparam.sSubKeys);
            //
            foreach (string sName in serialparam.sSubKeys)
            {

                string sValue = (string)keyCom.GetValue(sName);
                serialparam.Cominfo.Add(sValue, new string[] { "115200", "8", "None", "1", "1" });//"115200,8,None,1,1");
                serialparam.Coms.Add(sValue, new SerialPort());

            }
        }

        private void ButtonFechar_Click(object sender, RoutedEventArgs e)
        {
            //shutdown
            Application.Current.Shutdown();
        }

        private void ListViewMenu_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            int index = ListViewMenu.SelectedIndex;
            MoveCursorMenu(index);

            if (setcompagetoelse == 0 & index != 0)
            {
                updateserialparam();
                setserialcomport();
            }

            switch (index)
            {
                case 0:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(formsetparm);
                    CMDtable.Visibility = Visibility.Visible;
                    break;
                case 1:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(formschadule);
                    CMDtable.Visibility = Visibility.Hidden;
                    break;
                case 2:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(new UserControlEscolha());
                    CMDtable.Visibility = Visibility.Hidden;
                    break;
                case 3:
                    GridPrincipal.Children.Clear();
                    GridPrincipal.Children.Add(new UserControlEscolha());
                    CMDtable.Visibility = Visibility.Hidden;
                    break;
                default:
                    break;

            }
            setcompagetoelse = index;
        }
        private void updateserialparam()
        {
            // keep the parms be reset.
            if (serialparam.toyo_name != null & serialparam.arduino_name != null)
            {
                if (serialparam.Cominfo[serialparam.toyo_name] != null)
                    serialparam.Cominfo[serialparam.toyo_name][0] = serialparam.toyo_baudrate.ToString();
                if (serialparam.Cominfo[serialparam.arduino_name][0] != null)
                    serialparam.Cominfo[serialparam.arduino_name][0] = serialparam.arduino_baudrate.ToString();
                if (serialparam.Cominfo[serialparam.toyo_name] == null)
                    serialparam.Cominfo.Add(serialparam.toyo_name, new string[] { "115200", "8", "None", "1", "1" });
                if (serialparam.Cominfo[serialparam.arduino_name][0] == null)
                    serialparam.Cominfo.Add(serialparam.arduino_name, new string[] { "115200", "8", "None", "1", "1" });

            }

        }
        /// <summary>
        /// the code produce go back to the origin point
        /// <example>
        /// <code>
        /// COM15
        /// </code>
        /// code>
        /// </code>
        /// </example>
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        ///
        private void bt_ORG_Click(object sender, RoutedEventArgs e)
        {
            string _selectcom = serialparam.toyo_name;
            int _delay = 0;
            string _return = null;
            MessageBox.Show(scriptCMD.ORG);
            Send_data(scriptCMD.ORG, _selectcom, _delay, _return, false);
        }

        private void setserialcomport()
        {
            #region serial port setting           
            if (serialparam.toyo_name != null & serialparam.arduino_name != null)
            {
                foreach (string sName in serialparam.sSubKeys)
                {
                    //
                    try
                    {
                        if (serialparam.Coms[sName] == null)
                        {
                            serialparam.Coms[sName] = new SerialPort();

                            serialparam.Coms[sName].Close();
                            serialparam.Coms[sName].PortName = sName;
                            serialparam.Coms[sName].BaudRate = Convert.ToInt32(serialparam.Cominfo[sName][0]);
                            serialparam.Coms[sName].DataBits = Convert.ToInt32(serialparam.Cominfo[sName][1]);
                            serialparam.Coms[sName].Parity = Parity.None;
                            serialparam.Coms[sName].StopBits = StopBits.One;
                            serialparam.Coms[sName].ReceivedBytesThreshold = 1;
                            serialparam.Coms[sName].DtrEnable = true;                //藍芽沒這串不能用
                            serialparam.Coms[sName].RtsEnable = true;

                        }
                        else
                        {
                            throw (new System.IO.IOException("This computer has been used !"));
                        }

                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
                //let comport to be open
                if (serialparam.use_toyo)
                    serialparam.Coms[serialparam.toyo_name].Open();
                else if (!serialparam.use_toyo)
                    serialparam.Coms[serialparam.toyo_name].Close();
                else if (serialparam.use_arduino)
                    serialparam.Coms[serialparam.arduino_name].Open();
                else if (!serialparam.use_arduino)
                    serialparam.Coms[serialparam.arduino_name].Close();
            }

            #endregion
        }

        private void MoveCursorMenu(int index)
        {
            TrainsitionigContentSlide.OnApplyTemplate();
            GridCursor.Margin = new Thickness(0, (200 + (60 * index)), 0, 0);
        }


        public  void ButtonSet_Click(object sender, RoutedEventArgs e)
        {

            //uint x = 0b_0001;
            //uint y = 0b_0010;

            //int a = 11;

            ////MessageBox.Show(Setstationmsg(a));
            ////MessageBox.Show((x ^= y).ToString());
            ////string msgcmd = "30 31 30 36 32 30 31 45 30 30 30 33";
            //string msgcmd = "30 31 30 36 32 30 31 45 30 30 30 33 ";//a2 0d
            // 創建線程執行耗時的任務

        }

        List<resultdataformat> items = new List<resultdataformat>();
        void _timer_Tick(object sender, EventArgs e)
        {


        }

        public static byte[] HexStringToBytes(string hs)
        {
            byte[] a = { 0x00 };            //非靜態變量處理
            try
            {
                //byte[] mid = Encoding.Default.GetBytes(hs);
                string[] strArr = hs.Trim().Split(' ');
                byte[] b = new byte[strArr.Length];
                //逐字變為16進制

                foreach (var word in strArr.Select((value, index) => new { value, index }))
                {
                    b[word.index] = Convert.ToByte(strArr[word.index].PadLeft(2, '0'), 16);
                }
                return b;
            }
            catch (Exception)
            {
                MessageBox.Show("重新確認輸入的16進制(please check format that the command input)");
                return a;
            }
        }

        public void send_sknot(string data)
        {
            time = DateTime.Now.TimeOfDay.ToString();
            time = time.Substring(0, 11);
            SendRecMsgstring = string.Format("{0} >> :{1} \r\n", time, data);
            ReceiveTextlist.Items.Add(SendRecMsgstring);
            ReceiveTextlist.SelectedIndex = ReceiveTextlist.Items.Count - 1;

        }

        private void bt_SendCMD_Click(object sender, RoutedEventArgs e)
        {
            int rows = 0;
            ///isopen?家寫port的判斷
            //
            do
            {
                if (scriptCMD.datares[rows].Contains("COM"))
                {
                    string[] subcmd = scriptCMD.datares[rows].Split(',');
                    int _delay = 0;
                    string _return = null;

                    Send_data(scriptCMD.datares[rows], subcmd[0], _delay, _return);
                }
                else
                {

                    string _selectcom = serialparam.toyo_name;
                    int _delay = 0;
                    string _return = null;
                    Send_data(scriptCMD.ORG, _selectcom, _delay, _return);

                }


                rows++;
            } while (rows < scriptCMD.datares[rows].Length);

            //
            //using (StringReader sr = new StringReader(WritetextTable.Text.Trim()))
            //{
            //    int WritetextTablerows = 0;
            //    do
            //    {
            //        strLineData = sr.ReadLine();
            //        if (!String.IsNullOrEmpty(strLineData))
            //            dinosaurs.Add(strLineData);
            //        WritetextTablerows++;

            //    } while (WritetextTablerows < row_Length);
            //}
            //int rows = 0;

            //// 
            //do
            //{
            //    if (dinosaurs[rows].Contains("COM"))
            //    {
            //        string[] subcmd = dinosaurs[rows].Split(',');
            //        int _delay = 0;
            //        string _return = null;
            //        if (auto_send == false) send_data(dinosaurs[rows], send_hex, subcmd[0], _delay, _return);
            //    }
            //    else
            //    {

            //        if (mcl_comlist.SelectedItem is null)
            //        {
            //            MetroFramework.MetroMessageBox.Show(this, "請選擇一個COM，進行CAN的設定", "錯誤提醒", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //        }
            //        else
            //        {
            //            string _selectcom = mcl_comlist.SelectedItem.ToString();
            //            int _delay = 0;
            //            string _return = null;
            //            //the com* be selected
            //            string selectcom = mcl_comlist.SelectedItem.ToString();
            //            send_data(dinosaurs[rows], send_hex, _selectcom, _delay, _return);
            //            //if (auto_send == false) send_data(dinosaurs[rows], send_hex);
            //        }
            //    }


            //    rows++;
            //} while (rows < dinosaurs.Count);

        }
        public class resultdataformat
        {
            public string time { get; set; }
            public int loop { get; set; }
            public int stride { get; set; }
            public string parm1 { get; set; }
            public string note { get; set; }
        }

        /// <summary>
        /// let msg into the method then procesing msg, and sending the msg to Toyo or other serial
        /// <see href= "https://igouist.github.io/post/2020/07/code-style/">良好寫手備忘錄</see>
        /// <para><typeparamref name="string"/> , <paramref name="data"/>  msge</para>
        /// <para><typeparamref name="string"/> , <paramref name="_com"/>  msge</para>
        /// <para><typeparamref name="int"/> , <paramref name="_delay"/>  msge</para>
        /// <para><typeparamref name="string"/> , <paramref name="_return"/>  msge</para>
        /// </summary>
        public void Send_data(string data, string _com, int _delay = 0, string _return = null, bool debug = false)
        {
            string msg = null;

            msg = data;

            if (ishex)
                msg = string.Concat(msg, "0D 0A");
            else
                msg = string.Concat(msg + "\r\n");

            if (debug)
                MessageBox.Show(msg);
            else
            {
                try
                {

                    if (serialparam.Coms[_com].IsOpen | Simu_debug == true)
                    {
                        //半段是否是hex
                        if (ishex)
                        {
                            byte[] hex = HexStringToBytes(msg);
                            serialparam.Coms[_com].Write(hex, 0, hex.Length);
                        }
                        else
                        {
                            byte[] UTF8bytes = Encoding.UTF8.GetBytes(msg);
                            serialparam.Coms[_com].Write(UTF8bytes, 0, UTF8bytes.Length);

                        }
                        //sleepmoment_CMD(_delay); //避免傳送的資料被打斷
                        //MessageBox.Show(_delay.ToString());

                        if (_return != null)
                            System.Threading.Thread.Sleep(_delay);                    //Task.Delay(11);

                        string time = DateTime.Now.TimeOfDay.ToString();
                        time = time.Substring(0, 11);

                        this.Dispatcher.Invoke((EventHandler)(delegate
                        {
                            send_sknot(msg);//📺←𝙁𝙏₂₃₂
                        }
                            )
                        );
                    }

                }
                catch (Exception ex)
                {
                    MessageBox.Show(this, "send_data error! ", "錯誤提醒");
                }

                //------ this part is reading feeback

                if (Simu_debug)
                {
                    System.Threading.Thread.Sleep(10);
                    string buffer = "";
                    if (serialparam.Coms[_com].IsOpen)
                    {
                        try
                        {
                            //等待數據进入缓冲區 不加延时可能发生数据分段現象
                            //System.Threading.Thread.Sleep(10);

                            int len = serialparam.Coms[_com].BytesToRead;
                            byte[] bytes = new byte[len];

                            serialparam.Coms[_com].Read(bytes, 0, len);

                            this.Dispatcher.Invoke((EventHandler)(delegate
                            {
                                if (ishex)
                                    buffer = byteToHexStr(bytes);                               //byte trans to hex
                                else
                                    buffer = System.Text.Encoding.Default.GetString(bytes);     //byte trans to ascii

                            }
                               )
                            );
                        }
                        catch (Exception ex)
                        {
                            MessageBox.Show(ex.Message);
                        }
                    }


                }


            }


        }

        /// <summary>
        /// send CMD to Toyo
        /// </summary>
        /// <param name="data">
        /// ajrdlwfjlkawnfkljanwlkfjew
        /// </param>
        /// <param name="_com">2</param>
        /// <param name="_delay">3</param>
        /// <param name="_return">4</param>

        public static string byteToHexStr(byte[] bytes)
        {
            string returnStr = "";
            if (bytes != null)
            {

                Parallel.ForEach(bytes, (d) =>
                {
                    returnStr += d.ToString("X2");
                    returnStr += d.ToString(" ");
                });
            }
            return returnStr.Trim();
        }
        public void Tostring(string buffer)
        {
            string[] partsS;

            if (buffer.Length > 25)
            {

                partsS = buffer.Split(' ');

                if (partsS[0] == "C3" && partsS[1] == "0D" && partsS[2] == "0A")//&& partsS[1] == "0D" && partsS[1] == "0A")
                {
                    //ReceiveTextlist.Items.Add("[!]" + " \r\n");
                    buffer = buffer.Replace("C3 0D 0A ", "");
                    partsS = buffer.Split(' ');
                    receive_sknot(buffer);

                }
                else
                {
                    buffer = buffer.Replace("C3 0D 0A ", "");
                    receive_sknot(buffer);
                }

            }
            else
            {
                receive_sknot(buffer);
            }

        }
        public void receive_sknot()
        {
            time = DateTime.Now.TimeOfDay.ToString();
            time = time.Substring(0, 11);
            SendRecMsgstring = string.Format("{0} << : \r\n", time);
            ReceiveTextlist.Items.Add(SendRecMsgstring);
            ReceiveTextlist.SelectedIndex = ReceiveTextlist.Items.Count - 1;
        }

        private void bt_sendCMDfromTB_Click(object sender, RoutedEventArgs e)
        {
            int row_Length = tb_CMDscript.LineCount;
            scriptCMD.datares = new List<string>();
            string strLineData;

            using (StringReader sr = new StringReader(tb_CMDscript.Text.Trim()))
            {
                int WritetextTablerows = 0;
                do
                {
                    strLineData = sr.ReadLine();
                    if (!String.IsNullOrEmpty(strLineData))
                    {
                        scriptCMD.datares.Add(strLineData);
                        //MessageBox.Show(strLineData);
                    }
                    WritetextTablerows++;
                } while (WritetextTablerows < row_Length);
            }

        }

        public void receive_sknot(string data)
        {
            time = DateTime.Now.TimeOfDay.ToString();
            time = time.Substring(0, 11);
            SendRecMsgstring = string.Format("{0} << :{1} \r\n", time, data);
            ReceiveTextlist.Items.Add(SendRecMsgstring);
            ReceiveTextlist.SelectedIndex = ReceiveTextlist.Items.Count - 1;
        }


        public void Run_scheduale()
        {
            string _selectcom = serialparam.toyo_name;


            int numstride =  scriptCMD.Dic_GetModemsg.Count;
            for (int i = 0; i < numstride; i++)
            {

                byte[] byte_GetModemsg = scriptCMD.Dic_GetModemsg[i];
                byte[] byte_GetDistancemsg = scriptCMD.Dic_GetDistancemsg[i];
                byte[] byte_GetSpeed = scriptCMD.Dic_GetSpeed[i];
                byte[] byte_GetDelaytime = scriptCMD.Dic_GetDelaytime[i];
                byte[] byte_GetNextStride = scriptCMD.Dic_GetNextStride[i];



                serialparam.Coms[_selectcom].Write(byte_GetModemsg, 0, byte_GetModemsg.Length);
                serialparam.Coms[_selectcom].Write(byte_GetDistancemsg, 0, byte_GetDistancemsg.Length);
                serialparam.Coms[_selectcom].Write(byte_GetSpeed, 0, byte_GetSpeed.Length);
                serialparam.Coms[_selectcom].Write(byte_GetDelaytime, 0, byte_GetDelaytime.Length);
                serialparam.Coms[_selectcom].Write(byte_GetNextStride, 0, byte_GetNextStride.Length);

            }
   


        }


    }


}



///
/// ref
///https://github.com/Wolfleader101/DinnerPlanner/blob/b55aeac590a5cb422714ed3a11b51055009b0743/MainWindow.xaml.cs