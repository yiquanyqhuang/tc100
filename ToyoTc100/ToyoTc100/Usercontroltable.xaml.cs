﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO;
using System.IO.Ports;
using Microsoft.Win32;

namespace ToyoTc100
{
    /// <summary>
    /// Usercontroltable.xaml 的互動邏輯
    /// </summary>
    public partial class Usercontroltable : UserControl
    {
        MainWindow main = App.Current.MainWindow as MainWindow;
        public string indebugpath = System.Environment.CurrentDirectory;
        public Usercontroltable()
        {
            InitializeComponent();
           
            if (Directory.Exists(indebugpath + "\\img") == false) Directory.CreateDirectory(indebugpath + @"\img");
            if (Directory.Exists(indebugpath + "\\util") == false) Directory.CreateDirectory(indebugpath + @"\util");
            if (Directory.Exists(indebugpath + "\\script") == false) Directory.CreateDirectory(indebugpath + @"\script");
            if (Directory.Exists(indebugpath + "\\param") == false) Directory.CreateDirectory(indebugpath + @"\param");
            if (Directory.Exists(indebugpath + "\\data") == false) Directory.CreateDirectory(indebugpath + @"\data");
            if (Directory.Exists(indebugpath + "\\param\\_run") == false) Directory.CreateDirectory(indebugpath + @"\param\_run");
            if (Directory.Exists(indebugpath + "\\param\\_pre") == false) Directory.CreateDirectory(indebugpath + @"\param\_pre");

            Searchcomport();

        }

        private void Searchcomport()
        {

            //
            cb_tc100com.Items.Clear();
            cb_arduinocom.Items.Clear();

            //
            RegistryKey keyCom = Registry.LocalMachine.OpenSubKey("Hardware\\DeviceMap\\SerialComm");
            string[] sSubKeys = keyCom.GetValueNames();
            //SerialPorts = new SerialPort[sSubKeys.Length];

            Array.Sort(sSubKeys);
            //
            foreach (string sName in sSubKeys)
            {
                string sValue = (string)keyCom.GetValue(sName);
                cb_tc100com.Items.Add(sValue);
                cb_arduinocom.Items.Add(sValue);

            }
            //
            cb_tc100com.SelectedIndex = 0;
            cb_arduinocom.SelectedIndex = 0;
        }


        private void asigncom( string _com, int _baudrate, Dictionary<string, SerialPort> _Coms)
        {


        }

        private void cb_tc100_Checked(object sender, RoutedEventArgs e)
        {

            string toyoname = cb_tc100com.Text;
            int baudrate = Convert.ToInt32(cb_tc100baudrate.Text);
            serialparam.toyo_name = toyoname;
            serialparam.toyo_baudrate = baudrate;

            if (cb_tc100coms.IsChecked == true)
                serialparam.use_toyo = true;
            if (cb_tc100coms.IsChecked == false)
                serialparam.use_toyo = false;

        }

        private void cb_arduino_Click(object sender, RoutedEventArgs e)
        {

            string arduinoname = cb_arduinocom.Text;
            int arduinobaudrate = Convert.ToInt32(cb_arduinocombaudrate.Text);
            serialparam.arduino_name = arduinoname;
            serialparam.arduino_baudrate = arduinobaudrate;

            if (cb_arduino.IsChecked ==true)
                serialparam.use_arduino = true;
            if (cb_arduino.IsChecked == false)
                serialparam.use_arduino = false;

        }

        private void bt_correctionpos_Click(object sender, RoutedEventArgs e)
        {


        }

        private void bt_sendCMDfromTB_Click(object sender, RoutedEventArgs e)
        {




        }
    }

}
