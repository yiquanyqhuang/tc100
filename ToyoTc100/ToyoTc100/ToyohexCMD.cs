﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Globalization;

namespace ToyoTc100
{
    public class ToyohexCMD
    {
        //03
        //起始碼 - 站號 - 功能碼 - 讀取位置 - 讀取word數量 - crc16
        //06
        //起始碼 - 站號 - 功能碼 - 讀取位置 - 讀取word數量 - crc16
        //10
        //起始碼 - 站號 - 功能碼 - 寫入開始位置 - 寫入word數量 - 寫入byte - word1 - word2 - crc16
        //起始碼
        public const string head = "3A ";
        public const string READ = "30 33 ";
        public const string WRITE = "30 36 ";
        public const string MACRO_WRITE = "31 30 ";
        public const string STATION = "30 31 ";

        public static string NumToHexWord(string _msg)
        {
            //32 30 31 45
            string result = String.Concat(_msg.ToUpper().Select(x => string.Concat(((int)x).ToString("x2"), " ")));
            return result;
        }
    }

    public class FuncPackage
    {

        /// <summary>
        /// 其實也可以REF https://home.gamer.com.tw/creationDetail.php?sn=4261522
        /// 不過我是沒用，因為太長了(無奈)
        /// </summary>
        /// <param name="pDataBytes"></param>
        /// <returns></returns>
        /// 
        public string headCRC16(byte[] pDataBytes)
        {
            ushort crc = 0xffff;
            ushort polynom = 0xA001;

            for (int i = 0; i < pDataBytes.Length; i++)
            {
                crc ^= pDataBytes[i];
                for (int j = 0; j < 8; j++)
                {
                    if ((crc & 0x01) == 0x01)
                    {
                        crc >>= 1;
                        crc ^= polynom;
                    }
                    else
                    {
                        crc >>= 1;
                    }
                }
            }
            //transfer the ushort to byte
            byte[] bytemsg = BitConverter.GetBytes(crc);
            string CRC16 = string.Concat(bytemsg.Select(b => string.Concat(b.ToString("X2"), " ")).ToArray());
            //
            string CMDCRC16 = string.Concat(ToyohexCMD.head, "", CRC16);
            return CMDCRC16.Trim();
        }

        /// <summary>
        /// the code produce that str"31 32 33 34 35 36" to "123456"
        /// </summary>
        /// <param name="msg"></param>
        /// <returns> </returns>
        public string StringTohex(string msg)
        {

            string[] hexValuesSplit = msg.Trim().Split(' ');

            StringBuilder hexbuffer = new StringBuilder();
            IEnumerable<string> squares = Enumerable.Range(0, hexValuesSplit.Length).Select(x => Char.ConvertFromUtf32(Convert.ToInt32(hexValuesSplit[x], 16)));
            foreach (string num in squares)
            {
                hexbuffer.Append(num);
            }
            string hexstring = hexbuffer.ToString();//iwantthis
            return hexstring;
        }


        //Following converts the hex to a byte[]
        public byte[] ConvertHexadecimalStringToByteArray(string hexadecimalString)
        {
            if (hexadecimalString.Length % 2 != 0)
            {
                throw new ArgumentException(String.Format(CultureInfo.InvariantCulture, "HexaDecimal cannot have an odd number of digits: {0}", hexadecimalString));
            }

            byte[] hexByteArray = new byte[hexadecimalString.Length / 2];
            for (int index = 0; index < hexByteArray.Length; index++)
            {
                string byteValue = hexadecimalString.Substring(index * 2, 2);
                hexByteArray[index] = byte.Parse(byteValue, NumberStyles.HexNumber, CultureInfo.InvariantCulture);
            }

            return hexByteArray;
        }
           
        /// <summary>
        /// calculates the checksum from the byte[] 
        /// <see href="https://dotnetfiddle.net/LttSkN"/>
        /// <para https://www.scadacore.com/tools/programming-calculators/online-checksum-calculator/ > check tool </para>
        /// </summary>
        /// <param name="msg"></param>
        /// <returns></returns>
        /// 

        public string CheckSum82sComple(string msg)
        {
            string submsg = StringTohex(msg);
            byte[] byteData = ConvertHexadecimalStringToByteArray(submsg);

            byte fill = 0;
            string binary = null;
            for (int i = 0; i < byteData.Length; i++)
            {

                fill += byteData[i];
            }

            fill = (byte)(0 - fill);
            binary = fill.ToString("X2");
            byte[] bytes = Encoding.Default.GetBytes(binary);
            string hexString = BitConverter.ToString(bytes);
            hexString = hexString.Replace("-", " ");
            return ToyohexCMD.head + msg  + hexString + " ";
        }

    }

 





}


