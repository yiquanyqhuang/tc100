﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ToyoTc100
{
    class hexCMD
    {
        const string head = "3A ";
        const string READ = "30 33 ";
        const string WRITE = "30 36 ";
        const string marcro_WRITE = "31 30 ";
        const string STATION = "30 31 ";
        //0x3A, 0x30, 0x31, 0x30, 0x36, 0x32, 0x30, 0x31, 0x45, 0x30, 0x30, 0x30, 0x33, 0x42, 0x38, 0x0D, 0x0A 
        string ORG = string.Concat(head, STATION, WRITE, numtohexword("201E"));

        private static string numtohexword(string _msg)
        {
            //32 30 31 45
            string result = String.Concat(_msg.ToUpper().Select(x => string.Concat(((int)x).ToString("x2"), " ")));
            return result.Trim();
        }
    }


}
